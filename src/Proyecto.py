#! /usr/bin/env python
# encoding:UTF-8

'''
Inteligencia Artificial:

Proyecto realizado por Fernando-Enzo Guarini Tolón,
alumno de 3º de Ingeniería del Software.

En este proyecto se implementan los operadores de absorción, identificación (v-operadores),
interconstrucción e intraconstrucción (w-operadores) para la resolución inversa de reglas.

No se ha consultado bibliografía alguna para la realización de este programa.


Para la creación de funciones, proceder del siguiente modo:

f1 = Funcion("Nombre", Aridad, [Argumentos])

→ Si no se añade un atributo argumentos, éstos se rellenarán automáticamente con un
número de variables genéricas adecuado.

→ Los elementos de la lista de Argumentos pueden ser bien cadenas de texto que comiencen
por una mayúscula, en cuyo caso se considerarán como variables, bien cadenas de texto que 
sean números o comiencen por minúscula, donde se tomarán como constantes, bien otras funciones
definidas con anterioridad.


Para la definición de reglas, proceder del siguiente modo:

r1 = Regla("Nombre", Cabeza, [Cuerpo])

→ Tanto el atributo Cabeza como los elementos de la lista Cuerpo deben ser de tipo Funcion.

'''

# ------------------------------------------------------------------------------ #

class Variables:
    variables_creadas = {}
    variables_inversas = {}
    subindices = {"0": "₀", "1":"₁", "2":"₂", "3":"₃", "4":"₄",
                  "5": "₅", "6":"₆", "7":"₇", "8":"₈", "9":"₉"}
    abecedario = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                  "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T",
                  "U", "V", "W", "X", "Y", "Z"]
    subindices_inverso = dict(reversed(item) for item in subindices.items())
    
    def __init__(self):
        self.puntero = -1
    
    def getVariableABC(self):
        try:
            self.puntero += 1
            return Variables.abecedario[self.puntero]
        except:
            return None
    
    def getVariable():
        num = len(Variables.variables_creadas) + 1
        for i in range(1, num):
            if i not in Variables.variables_creadas:
                num = i
                break
        res = "X" + Variables.miniatura(num)
        Variables.variables_creadas[num] = res
        Variables.variables_inversas[res] = num
        return res
    
    def getVariableDefinida(numero):
        if (numero == 0):
            raise Exception("El número de la variable genérica no puede ser 0")
        try:
            return Variables.variables_creadas[numero]
        except KeyError:
            res = "X" + Variables.miniatura(numero)
            Variables.variables_creadas[numero] = res
            Variables.variables_inversas[res] = numero
            return res
    
    def miniatura(numero):
        cifras = [Variables.subindices[num] for num in str(numero)]
        res = ""
        for num in cifras:
            res += num
        return res


# ------------------------------------------------------------------------------ #

class Funcion:
    funciones_registradas = {}
    
    def __init__(self, nombre, aridad, valores=[]):
        try:
            Funcion.funciones_registradas[nombre]
        except:
            Funcion.funciones_registradas[nombre] = aridad
        if (Funcion.funciones_registradas[nombre] != aridad):
            raise Exception ("Ya existe una función «" + nombre + "» con distinta aridad.")
        self.nombre = nombre
        self.aridad = aridad
        self.valores = []
        self.tipo_variables = []
        self.representacion = ""
        self.__inicializar_variables()
        if (len(valores) != 0):
            self.__set_constantes(valores)
        else:
            self.valores = tuple(self.valores)
        self.__definir_variables()
    
    def __len__(self):
        return self.aridad
    
    def __inicializar_variables(self):
        self.valores = [Variables.getVariable() for _ in range(self.aridad)]
    
    def __definir_variables(self):
        for elem in self.valores:
            if isinstance(elem, Funcion):
                self.tipo_variables.append("func")
            elif elem[0].isupper():
                self.tipo_variables.append("var")
            else:
                self.tipo_variables.append("const")
        self.tipo_variables = tuple(self.tipo_variables)
    
    def __set_constantes(self, constantes):
        for i in range(len(constantes)):
            self.__set_constante(i, constantes[i])
        self.valores = tuple(self.valores)
    
    def __set_constante(self, indice, constante):
        self.valores = list(self.valores)
        if isinstance(constante, Funcion):
            self.valores[indice] = constante
        else:
            constante = str(constante)
            if constante.find("gen") == -1:
                self.valores[indice] = constante
            else:
                constante = constante.replace("gen", "")
                constante = constante.replace(" ", "")
                if constante == "":
                    if (self.valores[indice][0].isupper() == False):
                        self.valores[indice] = Variables.getVariable()
                else:
                    self.valores[indice] = Variables.getVariableDefinida(int(constante))
        self.representacion = ""
    
    def get_constantes(self):
        """Devuelve todas las constantes de la función, independientemente de
        su profundidad, y una variable candidata a sustituirlas."""
        res = {}
        i = 0
        for elem in self.tipo_variables:
            if elem == "const":
                res[self.valores[i]] = Variables.getVariable()
            elif elem == "func":
                res.update(self.valores[i].get_constantes())
            i += 1
        return res
    
    def get_variables(self):
        """Devuelve todas las variables de la función, independientemente de
        su profundidad."""
        res = []
        i = 0
        for elem in self.tipo_variables:
            if elem == "var":
                res.append(self.valores[i])
            elif elem == "func":
                res.extend(self.valores[i].get_variables())
            i += 1
        return sorted(list(set(res)))
    
    def unifica(func_a, func_b):
        """Devuelve una unificación entre la función a y la función b si existe.
        La unificación será del tipo var_a/var_b si ambas resultan variables."""
        if (func_a == func_b):
            return {}
        unificadores = {}
        mensaje_error = str(func_a) + " y " + str(func_b) + " no son unificables"
        if (func_a.nombre != func_b.nombre):
            raise Exception (mensaje_error)
        for i in range(len(func_a)):
            valor_a, valor_b = func_a.valores[i], func_b.valores[i]
            variable_a, variable_b = func_a.tipo_variables[i], func_b.tipo_variables[i]
            
            if (valor_a != valor_b):
                
                if variable_a == "const":
                    if variable_b == "const" or variable_b == "func":
                        raise Exception (mensaje_error)
                    
                    unificadores[valor_b] = valor_a
                
                elif variable_a == "var":
                    if variable_b == "func":
                        if Funcion.occur_check(valor_a, valor_b):
                            raise Exception (mensaje_error)
                    
                    unificadores[valor_a] = valor_b
                
                elif variable_a == "func":
                    if variable_b == "const":
                        raise Exception (mensaje_error)
                    
                    elif variable_b == "var":
                        if Funcion.occur_check(valor_b, valor_a):
                            raise Exception (mensaje_error)
                        else:
                            unificadores[valor_b] = valor_a
                    
                    elif variable_b == "func":
                        unificadores.update(Funcion.unifica(valor_a, valor_b))
                break
        
        f1 = func_a.aplicar_unificaciones(unificadores)
        f2 = func_b.aplicar_unificaciones(unificadores)
        unificadores.update(Funcion.unifica(f1, f2))
        unificadores = Funcion.normaliza_unificadores(unificadores)
        return unificadores
    
    def normaliza_unificadores(unificadores):
        """Comprueba que ninguna variable y/o constante aparezca en la clave
        del diccionario y en un valor a la vez (sustituye si es necesario)."""
        for var in unificadores:
            for var2 in unificadores:
                clave = unificadores[var2]
                if isinstance(clave, Funcion):
                    if Funcion.occur_check(var, clave):
                        unificadores[var2] = clave.aplicar_unificacion(var, unificadores[var])
                elif var == clave:
                    unificadores[var2] = unificadores[var]
        return unificadores
    
    def aplicar_unificacion(self, a, b):
        """Sustituye en la función todas las ocurrencias de A por B."""
        valores = list(self.valores)
        for i in range (len(valores)):
            if valores[i] == a:
                valores[i] = b
            elif self.tipo_variables[i] == "func":
                valores[i] = valores[i].aplicar_unificacion(a, b)
        return Funcion(self.nombre, self.aridad, valores)
    
    def aplicar_unificaciones(self, unificadores):
        """Aplica varios unificadores a la vez."""
        f1 = self
        for elem in unificadores:
            f1 = f1.aplicar_unificacion(elem, unificadores[elem])
        return f1
    
    def occur_check(variable, funcion):
        """Detecta si una variable aparece dentro de una función."""
        res = False
        for elem in funcion.valores:
            if res == True:
                return True
            elif variable == elem:
                return True
            elif isinstance(elem, Funcion):
                res = Funcion.occur_check(variable, elem)
        return res
        
    def __eq__(self, other):
        if isinstance(other, Funcion):
            if (self.nombre == other.nombre) and (self.aridad == other.aridad) and (self.valores == other.valores):
                return True
        return False
    
    def __ne__(self, other):
        return not self.__eq__(other)
    
    def __hash__(self):
        return hash(self.__str__())
    
    def __lt__(self, other):
        return self.nombre.__lt__(other.nombre)
    
    def __str__(self):
        if self.representacion == "":
            self.representacion += self.nombre + self.valores.__str__()
            self.representacion = self.representacion.replace("'", "")
            self.representacion = self.representacion.replace(",)", ")")
            self.representacion = self.representacion.replace("[", "(")
            self.representacion = self.representacion.replace("]", ")")
        return self.representacion
    
    def __repr__(self):
        return self.__str__()


# ------------------------------------------------------------------------------ #

class Regla:
    def __init__(self, nombre, cabeza, cuerpo=[]):
        self.nombre = nombre
        self.cabeza = cabeza
        self.cuerpo = cuerpo
        self.representacion = ""
        self.__asegurar_no_duplicados()
        self.cuerpo.sort()
        
    def get_constantes(self):
        constantes = self.cabeza.get_constantes()
        for elem in self.cuerpo:
            constantes.update(elem.get_constantes())
        return constantes
    
    def get_variables(self):
        variables = set(self.cabeza.get_variables())
        for elem in self.cuerpo:
            variables.union(set(elem.get_variables()))
        return sorted(list(variables))
    
    def __asegurar_no_duplicados(self):
        """Si en el cuerpo de la regla se detecta que una función aparece varias veces,
        se lanza una excepción."""
        for elem in self.cuerpo:
            i = 0
            for var in self.cuerpo:
                if (elem.nombre == var.nombre) and (elem != var):
                    i += 1
                if (i == 1):
                    raise Exception ("La función «" + elem.nombre + "» está duplicada en el cuerpo de la regla " + self.nombre + ".")
    
    def aplicar_unificaciones(self, unificaciones):
        nuevo_cuerpo = [elem.aplicar_unificaciones(unificaciones) for elem in self.cuerpo]
        nueva_cabeza = self.cabeza.aplicar_unificaciones(unificaciones)
        return Regla(self.nombre, nueva_cabeza, nuevo_cuerpo)

    def constantes_en_regla(constantes, regla):
        '''Devuelve True solamente si todas las constantes aparecen en la Regla.
        En caso contrario, devuelve False y una lista de las constantes que sí aparecen
        en la Regla.'''
        const_regla = regla.get_constantes().keys()
        constantes = set(constantes.keys())
        interseccion = set(constantes.intersection(const_regla))
        if (interseccion == constantes):
            return []
        return constantes.difference(interseccion)
    
    def renombrar_variables(self):
        """Renombra todas las variables de la regla a nombres genéricos y
        devuelve la regla con las nuevas variables y los cambios hechos."""
        variables = self.cabeza.get_variables()
        for func in self.cuerpo:
            variables.extend(func.get_variables())
        renombrados = {}
        for elem in variables:
            renombrados[elem] = Variables.getVariable()
        res = self.aplicar_unificaciones(renombrados)
        renombrados = dict(reversed(item) for item in renombrados.items())
        return [res, renombrados]
    
    def __renombrar_variables_facil(self):
        """Renombra todas las variables de la regla que sean del tipo X__ 
        a nombres fáciles de procesar humanamente. Función destinada a 
        ser llamada al final de la ejecución de un algoritmo."""
        generador = Variables()
        unificadores = {}
        variables = self.get_variables()
        for elem in variables:
            if (elem[0] == "X") and (len(elem)>1):
                while(True):
                    candidata = generador.getVariableABC()
                    if candidata not in variables:
                        unificadores[elem] = candidata
                        break
        return [self.aplicar_unificaciones(unificadores), unificadores]
    
    def reescribir_solucion(solucion, tipo="v-operador"):
        '''Reescribe una solución con todas sus variables fáciles de leer.'''
        res = list(solucion)
        try:
            if tipo == "w-operador":
                res[0] = res[0].__renombrar_variables_facil()[0]
                res[1] = res[1].__renombrar_variables_facil()[0]
                res[2] = res[2].__renombrar_variables_facil()[0]
            else:
                res[0] = res[0].__renombrar_variables_facil()
                for clave in res[0][1]:
                    valor = res[0][1][clave]
                    res[1][valor] = res[1][clave]
                    del res[1][clave]
                res[0] = res[0][0]
            return res
        except:
            raise Exception ("No se ha podido aplicar una reescritura de la solución.")
    
    def deshacer_renombrado(unificadores, renombrado):
        """Dado un mapa de variables renombradas y uno de variables candidatas
        a unificar, devuelve este última con todas las ocurrencias de
        «renombrado» sustituidas por un valor adecuado."""
        claves = list(renombrado.keys())
        unif_copia = dict(unificadores)
        
        for clave in claves:
            for key in unificadores:
                if key == clave:
                    unif_copia[renombrado[clave]] = unif_copia[clave]
                    del unif_copia[clave]
                elif unificadores[key] == clave:
                    unif_copia[key] = renombrado[clave]
        
        claves = list(unif_copia.keys())
        for clave in claves:
            if unif_copia[clave] == clave:
                del unif_copia[clave]
        
        unificadores = unif_copia
        return unificadores
        
    def v_operador(brazo, vertice):
        '''Primero probamos con absorción y, si no, con identificación.'''
        try:
            ausentes = Regla.constantes_en_regla(vertice.get_constantes(), brazo)
            res = Regla.v_operador_anonimo(Regla.absorcion, brazo, vertice, ausentes)
            res.append("absorcion")
            return res
        except:
            try:
                res = Regla.v_operador_anonimo(Regla.identificacion, brazo, vertice, ausentes)
                res.append("identificacion")
                return res
            except:
                raise Exception ("El v-operador no es aplicable a " + brazo.nombre + " y " + vertice.nombre + ".")

    def w_operador(b1, b2):
        '''Intentaremos aplicar primero intraconstrucción y, si no, interconstrucción.'''
        try:
            try:
                try:
                    res = Regla.intraconstruccion(b1, b2)
                    absor1 = Regla.v_operador(res[0], b1)
                    absor2 = Regla.v_operador(res[2], b2)
                    assert absor1[0] == absor2[0]
                    res.append("intraconstruccion")
                    return res
                except:
                    res = Regla.w_operador_anonimo(Regla.intraconstruccion, b1, b2)
                    absor1 = Regla.v_operador_anonimo(Regla.v_operador, res[0], b1)
                    absor2 = Regla.v_operador_anonimo(Regla.v_operador, res[2], b2)
                    assert absor1[0] == absor2[0]
                    res.append("intraconstruccion")
                    return res
            except:
                try:
                    res = Regla.interconstruccion(b1, b2)
                    ident1 = Regla.v_operador(res[0], b1)
                    ident2 = Regla.v_operador(res[2], b2)
                    assert ident1[0] == ident2[0]
                    res.append("interconstruccion")
                    return res
                except:
                    res = Regla.w_operador_anonimo(Regla.interconstruccion, b1, b2)
                    res = Regla.interconstruccion(b1, b2)
                    ident1 = Regla.v_operador(res[0], b1)
                    ident2 = Regla.v_operador_anonimo(Regla.v_operador, res[2], b2)
                    assert ident1[0] == ident2[0]
                    res.append("interconstruccion")
                    return res
        except:
            raise Exception ("El w-operador no es aplicable a " + b1.nombre + " y " + b2.nombre + ".")

    def v_operador_anonimo(operador, brazo, vertice, ausentes=[]):
        '''Intentamos unificar cambiando constantes por 
        variables antes de aplicar algún v-operador.'''
        unificadores = brazo.get_constantes()
        unificadores.update(vertice.get_constantes())
        unificadores = Funcion.normaliza_unificadores(unificadores)
        
        for elem in ausentes:
            del unificadores[elem]
        
        b1 = brazo.aplicar_unificaciones(unificadores)
        v1 = vertice.aplicar_unificaciones(unificadores)
        
        res = operador(b1, v1)
        res[1].update(dict(reversed(item) for item in unificadores.items()))
        return res

    def w_operador_anonimo(operador, b1, b2):
        '''Intentamos unificar cambiando constantes por 
        variables antes de aplicar algún w-operador.'''
        unificadores = b1.get_constantes()
        unificadores.update(b2.get_constantes())
            
        unificadores = Funcion.normaliza_unificadores(unificadores)
        
        b1_1 = b1.aplicar_unificaciones(unificadores)
        b2_1 = b2.aplicar_unificaciones(unificadores)
        
        res = operador(b1_1, b2_1)        
        return res
    
    def absorcion(brazo, vertice):
        mensaje_error = "No se puede aplicar el operador de absorción sobre " + brazo.nombre + " y " + vertice.nombre + "."
        elementos_unicos_brazo = Regla.__diferencias_cuerpos(brazo.cuerpo, vertice.cuerpo)
        if len(elementos_unicos_brazo[0]) != 0:
            raise Exception (mensaje_error)
        
        # Renombramos todas las variables del brazo. Luego desharemos los cambios.
        renombrado = brazo.renombrar_variables()
        brazo, renombrado = renombrado[0], renombrado[1]
        
        elementos_unicos_vertice = Regla.__diferencias_cuerpos(vertice.cuerpo, brazo.cuerpo)
        unificadores = elementos_unicos_vertice[1]
        
        var_unicas_cabeza_vertice = sorted(list(set([var for var in vertice.cabeza.get_variables() if var not in unificadores.keys()])))
        var_unicas_cabeza_brazo = sorted(list(set([var for var in brazo.cabeza.get_variables() if var not in unificadores.values()])))
        
        try:
            for i in range(len(var_unicas_cabeza_vertice)):
                unificadores[var_unicas_cabeza_vertice[i]] = var_unicas_cabeza_brazo[i]
        except:
            pass
        
        elementos_unicos_vertice = elementos_unicos_vertice[0]
        elementos_unicos_vertice.append(brazo.cabeza)
        unificadores = Funcion.normaliza_unificadores(unificadores)
        
        # Deshacemos el renombrado de variables
        unificadores = Regla.deshacer_renombrado(unificadores, renombrado)
        
        for i in range(len(elementos_unicos_vertice)):
            elementos_unicos_vertice[i] = elementos_unicos_vertice[i].aplicar_unificaciones(renombrado)
            elementos_unicos_vertice[i] = elementos_unicos_vertice[i].aplicar_unificaciones(unificadores)
        
        unif_copia = dict(unificadores)
        for i in unif_copia:
            if unificadores[i] == i:
                del unificadores[i] 
        
        nueva_cabeza = vertice.cabeza.aplicar_unificaciones(renombrado)
        nueva_cabeza = nueva_cabeza.aplicar_unificaciones(unificadores)
        return [Regla("C₂", nueva_cabeza, elementos_unicos_vertice), unificadores]
        
    def identificacion(brazo, vertice):
        mensaje_error = "No se puede aplicar el operador de identificación sobre " + brazo.nombre + " y " + vertice.nombre + "."
        
        # Renombramos todas las variables del brazo. Luego desharemos los cambios.
        renombrado = brazo.renombrar_variables()
        brazo, renombrado = renombrado[0], renombrado[1]
        
        try:
            # Es necesario obtener los unificadores de la cabeza y el cuerpo
            # por separado para asegurar la completitud.
            unificadores = Funcion.unifica(vertice.cabeza, brazo.cabeza)
            
            elementos_unicos_vertice = Regla.__diferencias_cuerpos(vertice.cuerpo, brazo.cuerpo)
            
            # Código para evitar que se sobreescriban claves
            for clave in elementos_unicos_vertice[1]:
                if clave in unificadores:
                    valor = unificadores[clave]
                    if (valor != elementos_unicos_vertice[1][clave]):
                        unificadores[valor] = clave
            unificadores.update(elementos_unicos_vertice[1])
            
            elementos_unicos_vertice = elementos_unicos_vertice[0] 
            elementos_unicos_brazo = Regla.__diferencias_cuerpos(brazo.cuerpo, vertice.cuerpo)[0]
            unificadores = Funcion.normaliza_unificadores(unificadores)
        
        except:
            raise Exception (mensaje_error)
        
        # Deshacemos el renombrado de variables
        unificadores = Regla.deshacer_renombrado(unificadores, renombrado)
        unificadores = Funcion.normaliza_unificadores(unificadores)
        
        nuevo_cuerpo = [funcion.aplicar_unificaciones(renombrado) for funcion in elementos_unicos_vertice]
        nueva_cabeza = [funcion.aplicar_unificaciones(renombrado) for funcion in elementos_unicos_brazo]
        
        assert len(nueva_cabeza) == 1
        
        res = [Regla("C₁", nueva_cabeza[0], nuevo_cuerpo), unificadores]
        res[0] = res[0].aplicar_unificaciones(unificadores)
        return res
    
    def intraconstruccion(b1, b2):
        mensaje_error = "No se puede aplicar el operador de intraconstrucción sobre " + b1.nombre + " y " + b2.nombre + "."
        unificadores = {}
        
        # Renombramos todas las variables de b1. Luego desharemos los cambios.
        renombrado = b1.renombrar_variables()
        b1, renombrado = renombrado[0], renombrado[1]
            
        try:
            
            # Es necesario obtener los unificadores de la cabeza y el cuerpo
            # por separado para asegurar la completitud.
            unificadores.update(Funcion.unifica(b2.cabeza, b1.cabeza))
            
            elementos_comunes = Regla.__comun_cuerpos(b2.cuerpo, b1.cuerpo)
            
            # Código para evitar que se sobreescriban claves
            for clave in elementos_comunes[1]:
                if clave in unificadores:
                    valor = unificadores[clave]
                    if (valor != elementos_comunes[1][clave]):
                        unificadores[valor] = clave
            unificadores.update(elementos_comunes[1])
            
            elementos_comunes = elementos_comunes[0]
            unificadores = Funcion.normaliza_unificadores(unificadores)
            
        except:
            raise Exception (mensaje_error)
        
        # Deshacemos el renombrado de variables
        unificadores = Regla.deshacer_renombrado(unificadores, renombrado)
        unificadores = Funcion.normaliza_unificadores(unificadores)
        
        nombre_q = Variables.getVariable().replace("X", "f")
        if (len(unificadores) != 0):
            cuerpo_q = list(unificadores.values())
        else:
            cuerpo_q = list(renombrado.values())
            if len(cuerpo_q) == 0:
                cuerpo_q = set()
                for elem in elementos_comunes:
                    cuerpo_q = cuerpo_q.union(set(elem.get_constantes().keys()))
                if len(cuerpo_q) == 0:
                    cuerpo_q = cuerpo_q.union(set(b1.cabeza.get_constantes().keys()))
                    cuerpo_q = cuerpo_q.union(set(b2.cabeza.get_constantes().keys()))
                if len(cuerpo_q) == 0:
                    cuerpo_q = cuerpo_q.union(set(b2.cabeza.get_variables()))
                    cuerpo_q = cuerpo_q.union(set(b1.cabeza.get_variables()))
                if len(cuerpo_q) == 0:
                    cuerpo_q.add(Variables.getVariable())
        q = Funcion(nombre_q, len(cuerpo_q), sorted(list(cuerpo_q)))
        
        cuerpo_c = list(elementos_comunes)
        cuerpo_c.append(q)
        
        C = Regla("C", b1.cabeza, cuerpo_c).aplicar_unificaciones(renombrado).aplicar_unificaciones(unificadores)
        B1 = b1.aplicar_unificaciones(renombrado).aplicar_unificaciones(unificadores)
        B2 = b2.aplicar_unificaciones(renombrado).aplicar_unificaciones(unificadores)
        C1 = Regla.v_operador(C, B1)
        C2 = Regla.v_operador(C, B2)
        C1[0].nombre = "C₁"
        C2[0].nombre = "C₂"
        unificadores.update(C1[1])
        unificadores.update(C2[1])
        
        try:
            assert C1[2] == "identificacion"
            assert C2[2] == "identificacion"
        except:
            raise Exception (mensaje_error)
        
        return [C1[0], C, C2[0]]
    
    def interconstruccion(b1, b2):
        mensaje_error = "No se puede aplicar el operador de interconstrucción sobre " + b1.nombre + " y " + b2.nombre + "."
        
        # Renombramos todas las variables de b1. Luego desharemos los cambios.
        renombrado = b1.renombrar_variables()
        b1, renombrado = renombrado[0], renombrado[1]
        
        try:
            elementos_comunes = Regla.__comun_cuerpos(b2.cuerpo, b1.cuerpo)
            unificadores = elementos_comunes[1]
            var_unicas_cabeza_b2 = sorted(list(set([var for var in b2.cabeza.get_variables() if var not in unificadores.keys()])))
            var_unicas_cabeza_b1 = sorted(list(set([var for var in b1.cabeza.get_variables() if var not in unificadores.values()])))
            try:
                for i in range(len(var_unicas_cabeza_b2)):
                    unificadores[var_unicas_cabeza_b2[i]] = var_unicas_cabeza_b1[i]
            except:
                pass
        except:
            raise Exception (mensaje_error)
        
        elementos_comunes = elementos_comunes[0]
        unificadores = Funcion.normaliza_unificadores(unificadores)
        
        # Deshacemos el renombrado de variables
        unificadores = Regla.deshacer_renombrado(unificadores, renombrado)
        unificadores = Funcion.normaliza_unificadores(unificadores)
        
        nombre_r = Variables.getVariable().replace("X", "f")
        if (len(unificadores) != 0):
            r = Funcion(nombre_r, len(unificadores), list(unificadores.values()))
        else:
            cuerpo = list(renombrado.values())
            if len(cuerpo) == 0:
                cuerpo = set()
                for elem in elementos_comunes:
                    cuerpo = cuerpo.union(set(elem.get_constantes().keys()))
                if len(cuerpo) == 0:
                    cuerpo = cuerpo.union(set(b1.cabeza.get_constantes().keys()))
                    cuerpo = cuerpo.union(set(b2.cabeza.get_constantes().keys()))
                if len(cuerpo) == 0:
                    cuerpo = cuerpo.union(set(b2.cabeza.get_variables()))
                    cuerpo = cuerpo.union(set(b1.cabeza.get_variables()))
                if len(cuerpo) == 0:
                    cuerpo.add(Variables.getVariable())
            r = Funcion(nombre_r, len(cuerpo), list(cuerpo))
        
        cuerpo_c = list(elementos_comunes)
        
        C = Regla("C", r, cuerpo_c).aplicar_unificaciones(renombrado).aplicar_unificaciones(unificadores)
        B1 = b1.aplicar_unificaciones(renombrado).aplicar_unificaciones(unificadores)
        B2 = b2.aplicar_unificaciones(renombrado).aplicar_unificaciones(unificadores)
        C1 = Regla.v_operador(C, B1)
        C2 = Regla.v_operador(C, B2)
        C1[0].nombre = "C₁"
        C2[0].nombre = "C₂"
        unificadores.update(C1[1])
        unificadores.update(C2[1])
        
        try:
            assert C1[2] == "absorcion"
            assert C2[2] == "absorcion"
        except:
            raise Exception (mensaje_error)
        
        return [C1[0], C, C2[0]]
    
    def __diferencias_cuerpos(cuerpo1, cuerpo2):
        """Devuelve los elementos que aparecen en la primera lista
        pero no en la segunda. También devuelve todas las unificaciones
        posibles entre cuerpo1 y cuerpo2."""
        res = []
        unificadores = {}
        for elem in cuerpo1:
            contenido = False
            for obj in cuerpo2:
                if (elem.nombre == obj.nombre):
                    unificadores.update(Funcion.unifica(elem, obj))
                    contenido = True
                    break
            if (contenido == False):
                res.append(elem)
        return [res, unificadores]
    
    def __comun_cuerpos(cuerpo1, cuerpo2):
        """Devuelve los elementos que aparecen en ambas listas.
        También devuelve todas las unificaciones posibles entre
        cuerpo1 y cuerpo2."""
        dif_cuerpos = Regla.__diferencias_cuerpos(cuerpo1, cuerpo2)
        lista = list(set(cuerpo1) - set(dif_cuerpos[0]))
        return [lista, dif_cuerpos[1]]
    
    def __eq__(self, other):
        if isinstance(other, Regla):
            try:
                if (len(self.cuerpo) == len(other.cuerpo)):
                    if (self.cabeza.get_constantes().keys() == other.cabeza.get_constantes().keys()):
                        unificadores = Funcion.unifica(self.cabeza, other.cabeza)
                        for i in range(len(self.cuerpo)):
                            unif = Funcion.unifica(self.cuerpo[i], other.cuerpo[i])
                            for clave in unif:
                                if clave in unificadores:
                                    valor = unificadores[clave]
                                    if (valor != unif[clave]):
                                        return False
                            unificadores.update(unif)
                        return True
            except:
                pass
        return False
    
    def __str__(self):
        if self.representacion == "":
            self.representacion = str(self.nombre) + " ≡ "
            if (self.cuerpo != None) and (len(self.cuerpo) > 0):
                self.representacion += str(self.cuerpo) + " → "
                self.representacion = self.representacion.replace("'", "")
                self.representacion = self.representacion.replace("[", "")
                self.representacion = self.representacion.replace("]", "")
            self.representacion += str(self.cabeza)
        return self.representacion
    
    def __repr__(self):
        return self.__str__()
            

# ------------------------------------------------------------------------------ #


"""EJEMPLO PROPUESTO EN EL ENUNCIADO PARA PROGENITOR E HIJO"""

madre = Funcion("madre", 2, ["X", "Y"])
progenitor = Funcion("progenitor", 2, ["X", "Y"])
c1 = Regla("C₁", progenitor, [madre])

mujer = Funcion("mujer", 1, ["Y"])
hija = Funcion("hija", 2, ["Y", "X"])
c2 = Regla("C₂", hija, [progenitor, mujer])

maria = Funcion("hija", 2, ["B", "A"])
madmaria = Funcion("madre", 2, ["A", "B"])
mujmaria = Funcion("mujer", 1, ["B"])

c = Regla("C", maria, [madmaria, mujmaria])

print(str(c1) + "\n" + str(c2) + "\n" + str(c) + "\n")
solucion = Regla.v_operador(c2, c)
print("\n" + str(Regla.reescribir_solucion(solucion, "v-operador")))

# ------------------------------------------------------------------------------ #

"""EJEMPLO PROPUESTO EN EL ENUNCIADO PARA EL NATURAL DE 0"""
print("\n\n\n")

nat0 = Funcion("natural", 1, ["0"])
r1 = Regla("C₁", nat0)

sig1 = Funcion("s", 1, ["X"])
natX = Funcion("natural", 1, ["X"])
nat1 = Funcion("natural", 1, [sig1])
r2 = Regla("C₂", nat1, [natX])

sig2 = Funcion("s", 1, ["0"])
nat2 = Funcion("natural", 1, [sig2])
r = Regla("C", nat2)

print(str(r1) + "\n" + str(r2) + "\n" + str(r) + "\n")
solucion = Regla.v_operador(r1, r)
print("\n" + str(Regla.reescribir_solucion(solucion, "v-operador")))

# ------------------------------------------------------------------------------ #

"""EJEMPLO PROPUESTO EN EL ENUNCIADO PARA INTRACONSTRUCCIÓN"""
print("\n\n\n")

desc = Funcion("descendiente", 2, ["X", "Y"])
mujer = Funcion("mujer", 1, ["X"])
madre = Funcion("madre", 2, ["Y", "X"])
padre = Funcion("padre", 2, ["Y", "X"])

b1 = Regla("B₁", desc, [mujer, madre])
b2 = Regla("B₂", desc, [mujer, padre])

print(str(b1) + "\n" + str(b2))
solucion = Regla.w_operador(b1, b2)
print("\n" + str(Regla.reescribir_solucion(solucion, "w-operador")))

# ------------------------------------------------------------------------------ #

"""EJEMPLO PROPUESTO EN EL ENUNCIADO PARA INTERCONSTRUCCIÓN"""
print("\n\n\n")

sig = Funcion("s", 1, "0")
b1 = Regla("B₁", Funcion("nat", 1, [sig]))
b2 = Regla("B₂", Funcion("nat", 1, [Funcion("s", 1, [sig])]))

print(str(b1) + "\n" + str(b2))
solucion = Regla.w_operador(b1, b2)
print("\n" + str(Regla.reescribir_solucion(solucion, "w-operador")))

# ------------------------------------------------------------------------------ #

"""SEGUNDO EJEMPLO PARA V-OPERADOR"""
print("\n\n\n")

enar = Funcion("estudiante", 1, ["enar"])
medica = Funcion("matriculado", 2, ["medicina", "enar"])
c1 = Regla("C₁", enar)

estudiante = Funcion("estudiante", 1, ["A"])
matriculado = Funcion("matriculado", 2, ["B", "A"])
c2 = Regla("C₂", matriculado, [estudiante])

c = Regla("C", medica)

print(str(c1) + "\n" + str(c2) + "\n" + str(c) + "\n")
solucion = Regla.v_operador(c2, c)
print("\n" + str(Regla.reescribir_solucion(solucion, "v-operador")))

# ------------------------------------------------------------------------------ #

"""SEGUNDO EJEMPLO PARA W-OPERADOR"""
print("\n\n\n")

captura = Funcion("captura_el_rey", 2, ["andres", "fernando"])
mejor = Funcion("juega_mejor", 2, ["andres", "fernando"])
peor = Funcion("juega_peor", 2, ["andres", "fernando"])
pierde = Funcion("pierde_la_partida", 1, ["andres"])

b1 = Regla("B₁", pierde, [captura, mejor])
b2 = Regla("B₂", pierde, [captura, peor])

print(str(b1) + "\n" + str(b2))
solucion = Regla.w_operador(b1, b2)
print("\n" + str(Regla.reescribir_solucion(solucion, "w-operador")))

# ------------------------------------------------------------------------------ #
